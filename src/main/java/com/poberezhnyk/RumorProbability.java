package com.poberezhnyk;

import java.util.ArrayList;

/**
 * An instance of this class is used to calculate probability that everyone will hear the rumor and expected number of guests to hear the rumor.
 *
 * @author Andrii Poberezhnyk
 *
 */
public class RumorProbability {
    /**
     * The number of guests which we got from com.poberezhnyk.Main.class.
     */
    private int num;
    /**
     * The number of attempts which we got from com.poberezhnyk.Main.class.
     */
    private int attempts;
    /**
     * Calculated chance to hear the rumor.
     */
    private double probability;
    /**
     * The amount of successful attempts after all attempts.
     */
    private int successfulAttempts;
    /**
     * Contains number of people who heard the rumor in every attempt.
     */
    private ArrayList<Integer> guestsHeardRumorFromEachTry = new ArrayList<Integer>();
    /** Creates a rumorProbability with the specified name.
     * @param num The number of guests.
     * @param attempts The number of attempts.
     */
    public RumorProbability(int num, int attempts) {
        this.num = num;
        this.attempts = attempts;
    }
    /** Calculates probability to hear the rumor after all attempts.
     * @since 1.0
     */
    public void calculateProbability(){
        successfulAttempts = 0;
        for (int i = 1; i <= attempts; i++) {
            if (calculateOneAttempt()) {
                successfulAttempts++;
            }
        }
        probability = (successfulAttempts * 1.0 / attempts) * 100;
        System.out.print("Probability that everyone at the party will hear the rumor is: ");
        System.out.printf("%.5f%s%n", probability, "%");
    }
    /** Calculates expected number of guests to hear the rumor.
     * @since 1.0
     */
    public void calculateAvgNumberOfPeopleHeardRumor() {
        int heardRumorCounter = 0;
        for (Integer i : guestsHeardRumorFromEachTry) {
            heardRumorCounter += i.intValue();
        }
        double avg = heardRumorCounter * 1.0 / attempts;
        System.out.println("Expected number of people to hear the rumor is: " + avg);
    }
    /** Checks if every guest will hear the rumor and calculates expected number of guests to hear the rumor after one attempt.
     * @return Returns true if every guest will hear the rumor.
     * @since 1.0
     */
    private boolean calculateOneAttempt() {
        ArrayList<Integer> heardRumor = new ArrayList<Integer>();
        int nextGuest = 1;
        int currentGuest = 1;
        heardRumor.add(nextGuest);
        for (int i = 1; i < num; i++) {
            nextGuest = randomGuest(currentGuest);
            if (heardRumor.contains(nextGuest)) {
                guestsHeardRumorFromEachTry.add(heardRumor.size());
                return false;
            } else {
                heardRumor.add(nextGuest);
            }
        }
        if (heardRumor.size() == num) {
            guestsHeardRumorFromEachTry.add(heardRumor.size());
            return true;
        } else {
            return false;
        }
    }
    /** Returns a pseudorandom int value of guest that will hear the rumor between 1 (inclusive) and the number of guests (inclusive).
     * @param current The number of guest who tells the rumor.
     * @return Returns the number of guest that will hear the rumor next.
     * @since 1.0
     */
    private int randomGuest(int current) {
        int res = 1 + (int) (Math.random() * num);
        while (res == current) {
            res = 1 + (int) (Math.random() * num);
        }
        return res;
    }
}
