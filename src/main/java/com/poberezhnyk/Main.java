package com.poberezhnyk;

import java.util.Scanner;

/**
 * Main class from where program starts.
 *
 * @author Andrii Poberezhnyk
 * @version 1.0
 */
public class Main {

    /**
     * Main method. Gets the number of guests and attempts from user.
     * Prints probability chance that everyone will hear the rumor and expected number of guests to hear the rumor.
     */
    public static void main(String[] args) {
        /**
         * The number of guests.
         */
        int num;
        /**
         * The number of attempts.
         */
        int attempts;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter number of guests: ");
        num = sc.nextInt();
        while (num < 3) {
            System.out.println("Invite more people and enter below:");
            num = sc.nextInt();
        }
        System.out.println("Enter number of attempts to increase accuracy (bigger -> better): ");
        attempts = sc.nextInt();
        while (attempts < 1) {
            System.out.println("Number should be above zero: ");
            attempts = sc.nextInt();
        }

        RumorProbability rumorProbability = new RumorProbability(num, attempts);
        rumorProbability.calculateProbability();
        rumorProbability.calculateAvgNumberOfPeopleHeardRumor();
    }
}
